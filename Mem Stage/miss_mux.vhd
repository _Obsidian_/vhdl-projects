library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity miss_mux is
    port(
        miss_sel:       in  std_logic;
        cache_addr:     in  std_logic_vector(9 downto 0);
        alu_addr:       in  std_logic_vector(31 downto 0);
        addr_out:       out std_logic_vector(9 downto 0)
    );
end miss_mux;

architecture Behavioral of miss_mux is

    signal alu_adr:         std_logic_vector(9 downto 0)            := "0000000000";

begin

    alu_adr   <= alu_addr(11 downto 2);

    addr_out <= alu_adr when (miss_sel = '0') else cache_addr;

end Behavioral;
