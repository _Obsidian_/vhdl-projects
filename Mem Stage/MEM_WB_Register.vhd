library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity MEM_WB_Register is
    port(
        clk:            in  std_logic;
        reset:          in  std_logic;
        rd_wr:          in  std_logic;
        mem_to_reg:     in  std_logic;
        wb_rd:          in  std_logic_vector(4 downto 0);
        alu_res:        in  std_logic_vector(31 downto 0);
        mem_data:       in  std_logic_vector(31 downto 0);
        rd_wr_out:      out std_logic;
        mem_to_reg_out: out std_logic;
        wb_rd_out:      out std_logic_vector(4 downto 0);
        alu_res_out:    out std_logic_vector(31 downto 0);
        mem_data_out:   out std_logic_vector(31 downto 0)
    );
end MEM_WB_Register;

architecture Behavioral of MEM_WB_Register is

    signal rd_wr_buff:          std_logic                           := '0';
    signal mem_to_reg_buff:     std_logic                           := '0';
    signal wb_rd_buff:          std_logic_vector(4 downto 0)        := "00000";
    signal alu_res_buff:        std_logic_vector(31 downto 0)       := x"00000000";
    signal mem_data_buff:       std_logic_vector(31 downto 0)       := x"00000000";

begin

    process(clk, reset)
        begin
            if(rising_edge(clk)) then
                rd_wr_buff      <= rd_wr;
                mem_to_reg_buff <= mem_to_reg;
                wb_rd_buff      <= wb_rd;
                alu_res_buff    <= alu_res;
                mem_data_buff   <= mem_data;
            end if;
    end process;

    rd_wr_out       <= rd_wr_buff when reset = '0' else '0';
    mem_to_reg_out  <= mem_to_reg_buff when reset = '0' else '0';
    wb_rd_out       <= wb_rd_buff when reset = '0' else (others => '0');
    alu_res_out     <= alu_res_buff when reset = '0' else (others => '0');
    mem_data_out    <= mem_data_buff when reset = '0' else (others => '0');

end Behavioral;
