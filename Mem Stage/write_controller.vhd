library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity write_controller is
    port(
        mem_op:         in  std_logic_vector(3 downto 0);
        alu_res:        in  std_logic_vector(31 downto 0);
        rs2_data:       in  std_logic_vector(31 downto 0);
        wr_enable:      out std_logic;
        addr_out:       out std_logic_vector(1 downto 0);
        we_out:         out std_logic_vector(3 downto 0);
        mem_data_out:   out std_logic_vector(31 downto 0)
    );
end write_controller;

architecture Behavioral of write_controller is

    signal addr:     std_logic_vector(1 downto 0)       := "00";

begin

    addr <= alu_res(1 downto 0);
    addr_out <= addr;
    
    process(mem_op, alu_res, rs2_data, addr)
        begin
            if(mem_op = "1010") then --sb
                wr_enable <= '1';
                case(addr) is
                    when "00" =>
                        mem_data_out <= rs2_data;
                        we_out       <= "0001";
                    when "01" =>
                        mem_data_out <= std_logic_vector(shift_left(signed(rs2_data), 8));
                        we_out       <= "0010";
                    when "10" =>
                        mem_data_out <= std_logic_vector(shift_left(signed(rs2_data), 10));
                        we_out       <= "0100";
                    when "11" =>
                        mem_data_out <= std_logic_vector(shift_left(signed(rs2_data), 18));
                        we_out       <= "1000";
                    when others =>
                        mem_data_out <= x"00000000";
                        we_out       <= "0000";
                end case;
            elsif(mem_op = "1001") then --sh
                wr_enable <= '1';
                case(addr) is
                    when "00" =>
                        mem_data_out <= std_logic_vector(shift_right(unsigned(rs2_data), 10));
                        we_out       <= "0011";
                    when "10" =>
                        mem_data_out <= std_logic_vector(shift_left(signed(rs2_data), 10));
                        we_out       <= "1100";
                    when others =>
                        mem_data_out <= x"00000000";
                        we_out       <= "0000";
                end case;
            elsif(mem_op = "1000") then --sw
                wr_enable <= '1';
                mem_data_out <= rs2_data;
                we_out       <= "1111";
            else                        --Not Writing
                wr_enable <= '0';
                mem_data_out <= x"00000000";
                we_out       <= "0000";
            end if;
     end process;

end Behavioral;
