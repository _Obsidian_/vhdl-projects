library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cache_controller is
    port(
        clk:            in  std_logic;
        reset:          in  std_logic;
        wr_in:          in  std_logic;
        addr_in:        in  std_logic_vector(31 downto 0);
        mem_data_in:    in  std_logic_vector(31 downto 0);
        mem_ena_out:    out std_logic;
        mem_addr_out:   out std_logic_vector(9 downto 0);
        cache_data_out: out std_logic_vector(31 downto 0)
    );
end cache_controller;

architecture Behavioral of cache_controller is

    component tag_compare is
        port(
            clk:        in  std_logic;
            reset:      in  std_logic;
            wr:         in  std_logic;
            addr_in:    in  std_logic_vector(31 downto 0);
            tag_out:    out std_logic_vector(6 downto 0);
            blk_addr:   out std_logic_vector(2 downto 0);
            addr_sel:   out std_logic;
            mem_ena:    out std_logic;
            count_ena:  out std_logic
           );
    end component;

    component addr_counter is
        port(
            clk:        in  std_logic;
            reset:      in  std_logic;
            ena_in:     in  std_logic;
            tag_in:     in  std_logic_vector(6 downto 0);
            mem_addr:   out std_logic_vector(9 downto 0);
            load_addr:  out std_logic_vector(2 downto 0)
           );
    end component;
    
    component cache_mux is
        port(
            sel:        in  std_logic;
            port_a:     in  std_logic_vector(2 downto 0);
            port_b:     in  std_logic_vector(2 downto 0);
            mux_out:    out std_logic_vector(2 downto 0)
           );
    end component;
    
    component cache_file is
        port(
            clk:            in  std_logic;
            reset:          in  std_logic;
            addr_in:        in  std_logic_vector(2 downto 0);
            wr_in:          in  std_logic;
            miss_in:        in  std_logic;
            mem_data_in:    in  std_logic_vector(31 downto 0);
            cache_data_out: out std_logic_vector(31 downto 0)
           );
    end component;
    
    signal tag_out_to_addr_count:       std_logic_vector(6 downto 0)            := "0000000";
    signal tag_comp_to_mux:             std_logic_vector(2 downto 0)            := "000";
    signal load_ctrl_to_mux:            std_logic                               := '0';
    signal load_ctrl_to_addr_count:     std_logic                               := '0';
    signal addr_count_to_mux:           std_logic_vector(2 downto 0)            := "000";
    signal mux_addr_to_cache:           std_logic_vector(2 downto 0)            := "000";
    signal miss:                        std_logic                               := '0';

begin

    mem_ena_out <= miss;

    tag_comp: tag_compare port map(
        clk             => clk,
        reset           => reset,
        wr              => wr_in,
        addr_in         => addr_in,
        tag_out         => tag_out_to_addr_count,
        blk_addr        => tag_comp_to_mux,
        addr_sel        => load_ctrl_to_mux,
        mem_ena         => miss,
        count_ena       => load_ctrl_to_addr_count
    );
       
    address_count: addr_counter port map(
        clk             => clk,
        reset           => reset,
        ena_in          => load_ctrl_to_addr_count,
        tag_in          => tag_out_to_addr_count,
        mem_addr        => mem_addr_out,
        load_addr       => addr_count_to_mux
       );
       
    cache_multi: cache_mux port map(
        sel             => load_ctrl_to_mux,
        port_a          => tag_comp_to_mux,
        port_b          => addr_count_to_mux,
        mux_out         => mux_addr_to_cache
       );

    cash_file: cache_file port map(
        clk             => clk,
        reset           => reset,
        addr_in         => mux_addr_to_cache,
        wr_in           => wr_in,
        miss_in         => miss,
        mem_data_in     => mem_data_in,
        cache_data_out  => cache_data_out
       );

end Behavioral;
