library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tag_compare is
    port(
        clk:        in  std_logic;
        reset:      in  std_logic;
        wr:         in  std_logic;
        addr_in:    in  std_logic_vector(31 downto 0);
        tag_out:    out std_logic_vector(6 downto 0);
        blk_addr:   out std_logic_vector(2 downto 0);
        addr_sel:   out std_logic;
        mem_ena:    out std_logic;
        count_ena:  out std_logic
    );
end tag_compare;

architecture Behavioral of tag_compare is

    signal tag:             std_logic_vector(6 downto 0)        := "0000000";
    signal tag_buff:        std_logic_vector(6 downto 0)        := "0000000";
    signal xor_tag:         std_logic_vector(6 downto 0)        := "0000000";
    signal b_addr:          std_logic_vector(2 downto 0)        := "000";
    signal binary_count:    unsigned(3 downto 0)                := "0000";
    signal reload_flag:     std_logic                           := '0';
    
    type STATE_TYPE is (s0, s1);
    signal state: STATE_TYPE := s0;

begin
    
    tag     <= addr_in(11 downto 5);
    b_addr  <= addr_in(4 downto 2);
    xor_tag <= tag xor tag_buff;
    tag_out <= tag_buff;

    state   <= s0 when (tag = tag_buff or wr = '0') and reload_flag = '0' else s1;

    process(clk, reset) --, tag, tag_buff, xor_tag, b_addr, binary_count, state)
    begin
        if(reset = '1') then
            tag_buff    <= "0010000";
            mem_ena     <= '0';
            count_ena   <= '0';
            addr_sel    <= '0';
            reload_flag <= '1';
        elsif(rising_edge(clk)) then
            if(xor_tag = "0000000" or state = s0) then
                blk_addr    <= b_addr;
                mem_ena     <= '0';
                count_ena   <= '0';
                addr_sel    <= '0';
                reload_flag <= '0';
            else
                mem_ena     <= '1';
                count_ena   <= '1';
                addr_sel    <= '1';
                reload_flag <= '1';
                binary_count    <= binary_count + 1;
                if(binary_count = "1000") then
                    binary_count    <= "0000";
                    reload_flag     <= '0';
                    tag_buff        <= tag;
               end if;
            end if;
       end if;
    end process;                    

end Behavioral;