library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cache_mux is
    port(
        sel:        in  std_logic;
        port_a:     in  std_logic_vector(2 downto 0);
        port_b:     in  std_logic_vector(2 downto 0);
        mux_out:    out std_logic_vector(2 downto 0)
    );
end cache_mux;

architecture Behavioral of cache_mux is

begin

    mux_out <= port_a when (sel = '0') else port_b;

end Behavioral;
