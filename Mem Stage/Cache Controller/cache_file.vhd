library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cache_file is
    port(
        clk:            in  std_logic;
        reset:          in  std_logic;
        addr_in:        in  std_logic_vector(2 downto 0);
        wr_in:          in  std_logic;
        miss_in:        in  std_logic;
        mem_data_in:    in  std_logic_vector(31 downto 0);
        cache_data_out: out std_logic_vector(31 downto 0)
       );
end cache_file;

architecture Behavioral of cache_file is

 type cache_type is array (0 to 7) of std_logic_vector (31 downto 0);
    
    signal cache_array:   cache_type  :=
        (x"00000000",     --X0
         x"00000000",     --X1
         x"00000000",     --X2
         x"00000000",     --X3
         x"00000000",     --X4
         x"00000000",     --X5
         x"00000000",     --X6
         x"00000000");    --X7    

begin
        
    process(clk)
        begin
            if(rising_edge(clk)) then
                if(wr_in = '1' or miss_in = '1') then 
                    cache_array(to_integer(unsigned(addr_in))) <= mem_data_in;
                end if;
            end if;
    end process;
    
    cache_data_out  <= cache_array(to_integer(unsigned(addr_in))) when (reset = '0') else (others => '0');
            
end Behavioral;