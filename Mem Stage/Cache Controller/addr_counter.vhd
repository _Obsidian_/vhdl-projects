library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity addr_counter is
    port(
        clk:        in  std_logic;
        reset:      in  std_logic;
        ena_in:     in  std_logic;
        tag_in:     in  std_logic_vector(6 downto 0);
        mem_addr:   out std_logic_vector(9 downto 0);
        load_addr:  out std_logic_vector(2 downto 0)
       );
end addr_counter;

architecture Behavioral of addr_counter is

    signal binary_counter:  unsigned(3 downto 0)    := (others => '0');

begin

    process(clk, reset, ena_in, binary_counter)
        begin
            if(reset = '1') then
                binary_counter <= (others => '0');
            elsif((ena_in = '0') or (binary_counter = "1000")) then
                binary_counter <= (others => '0');
            elsif(ena_in = '1') then
                if(rising_edge(clk)) then
                    binary_counter <= binary_counter + 1;
                end if;
            end if;
    end process;
    
    load_addr   <= std_logic_vector(binary_counter(2 downto 0));
    mem_addr    <= tag_in & std_logic_vector(binary_counter(2 downto 0));

end Behavioral;
