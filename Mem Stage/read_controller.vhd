library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity read_controller is
    port(
        addr:           in  std_logic_vector(1 downto 0);
        mem_op:         in  std_logic_vector(3 downto 0);
        cache_data:     in  std_logic_vector(31 downto 0);
        mem_data_out:   out std_logic_vector(31 downto 0)   
    );
end read_controller;

architecture Behavioral of read_controller is

    signal slv_32:          std_logic_vector(31 downto 0)           := x"00000000";
    
    signal mask_data_lb1:   std_logic_vector(23 downto 0)           := x"000000";
    signal res_data_lb1:    std_logic_vector(7 downto 0)            := x"00";
    
    signal mask_data_lb2:   std_logic_vector(15 downto 0)           := x"0000";
    signal res_data_lb2:    std_logic_vector(15 downto 0)           := x"0000";
    
    signal mask_data_lb3:   std_logic_vector(7 downto 0)            := x"00";
    signal res_data_lb3:    std_logic_vector(23 downto 0)           := x"000000";
    
    signal mask_data_lh1:   std_logic_vector(15 downto 0)           := x"0000";
    signal res_data_lh1:    std_logic_vector(15 downto 0)           := x"0000";
    
begin

    mask_data_lb1 <= cache_data(31 downto 8);
    res_data_lb1 <= cache_data(7 downto 0);

    mask_data_lb2 <= cache_data(31 downto 16);
    res_data_lb2 <= cache_data(15 downto 0);
    
    mask_data_lb3 <= cache_data(31 downto 24);
    res_data_lb3 <= cache_data(23 downto 0);
    
    mask_data_lh1 <= cache_data(31 downto 16);
    res_data_lh1 <= cache_data(15 downto 0);
    
    process(addr, mem_op, cache_data)
        begin
            if(mem_op = "0110") then --lb
                case(addr) is
                    when "00" =>
                        mem_data_out <= std_logic_vector(signed(((not mask_data_lb1) and mask_data_lb1) & res_data_lb1));
                    when "01" =>
                        mem_data_out <= std_logic_vector(shift_right(signed(((not mask_data_lb2) and mask_data_lb2) & res_data_lb2), 8));
                    when "10" =>
                        mem_data_out <= std_logic_vector(shift_right(signed(((not mask_data_lb3) and mask_data_lb3) & res_data_lb3), 10));
                    when "11" =>
                        mem_data_out <= std_logic_vector(shift_right(signed(cache_data), 18));
                    when others =>
                        mem_data_out <= x"00000000";
                end case;
            elsif(mem_op = "1110") then --lbu
                case(addr) is
                    when "00" =>
                        mem_data_out <= std_logic_vector(unsigned(((not mask_data_lb1) and mask_data_lb1) & res_data_lb1));
                    when "01" =>
                        mem_data_out <= std_logic_vector(shift_right(unsigned(((not mask_data_lb2) and mask_data_lb2) & res_data_lb2), 8));
                    when "10" =>
                        mem_data_out <= std_logic_vector(shift_right(unsigned(((not mask_data_lb3) and mask_data_lb3) & res_data_lb3), 10));
                    when "11" =>
                        mem_data_out <= std_logic_vector(shift_right(unsigned(cache_data), 18));
                    when others =>
                        mem_data_out <= x"00000000";
                end case;
            elsif(mem_op = "0101") then --lh
                case(addr) is
                    when "00" =>
                        mem_data_out <= std_logic_vector(signed(((not mask_data_lh1) and mask_data_lh1) & res_data_lh1));
                    when "10" =>
                        mem_data_out <= std_logic_vector(shift_right(signed(cache_data), 10));
                    when others =>
                        mem_data_out <= x"00000000";
                end case;
            elsif(mem_op = "1101") then --lhu
                case(addr) is
                    when "00" =>
                        mem_data_out <= std_logic_vector(unsigned(((not mask_data_lh1) and mask_data_lh1) & res_data_lh1));
                    when "10" =>
                        mem_data_out <= std_logic_vector(shift_right(unsigned(cache_data), 10));
                    when others =>
                        mem_data_out <= x"00000000";
                end case;
            elsif(mem_op = "0100") then --lw
                mem_data_out <= cache_data;
            else
                mem_data_out <= x"00000000";
            end if;
      end process;

end Behavioral;