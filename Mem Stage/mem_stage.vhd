library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mem_stage is
    port(
        clk:                    in  std_logic;
        reset:                  in  std_logic;
        exmem_rd_wr_in:         in  std_logic;
        exmem_mem_to_reg_in:    in  std_logic;
        exmem_mem_op_in:        in  std_logic_vector(3 downto 0);
        exmem_rd_in:            in  std_logic_vector(4 downto 0);
        exmem_alu_res_in:       in  std_logic_vector(31 downto 0);
        exmem_rs2_data_in:      in  std_logic_vector(31 downto 0);
        memmain_data_in:        in  std_logic_vector(31 downto 0);
        memmain_to_if:          out std_logic;
        memwb_mem_to_reg_out:   out std_logic;
        memwb_rd_wr_out:        out std_logic;
        memmain_enable:         out std_logic;
        memmain_we_out:         out std_logic_vector(3 downto 0);
        memwb_rd_out:           out std_logic_vector(4 downto 0);
        memmain_mux_addr_out:   out std_logic_vector(9 downto 0);
        memmain_data_out:       out std_logic_vector(31 downto 0);
        memwb_alu_res_out:      out std_logic_vector(31 downto 0);
        memwb_mem_data_out:     out std_logic_vector(31 downto 0)
    );
end mem_stage;

architecture Behavioral of mem_stage is

    component write_controller is
        port(
            mem_op:         in  std_logic_vector(3 downto 0);
            alu_res:        in  std_logic_vector(31 downto 0);
            rs2_data:       in  std_logic_vector(31 downto 0);
            wr_enable:      out std_logic;
            addr_out:       out std_logic_vector(1 downto 0);
            we_out:         out std_logic_vector(3 downto 0);
            mem_data_out:   out std_logic_vector(31 downto 0)
           );
    end component;
    
    component read_controller is
        port(
            addr:           in  std_logic_vector(1 downto 0);
            mem_op:         in  std_logic_vector(3 downto 0);
            cache_data:     in  std_logic_vector(31 downto 0);
            mem_data_out:   out std_logic_vector(31 downto 0)
           );
    end component;
    
    component miss_mux is
        port(
            miss_sel:       in  std_logic;
            cache_addr:     in  std_logic_vector(9 downto 0);
            alu_addr:       in  std_logic_vector(31 downto 0);
            addr_out:       out std_logic_vector(9 downto 0)
           );
    end component;
    
    component cache_controller is
        port(
            clk:            in  std_logic;
            reset:          in  std_logic;
            wr_in:          in  std_logic;
            addr_in:        in  std_logic_vector(31 downto 0);
            mem_data_in:    in  std_logic_vector(31 downto 0);
            mem_ena_out:    out std_logic;
            mem_addr_out:   out std_logic_vector(9 downto 0);
            cache_data_out: out std_logic_vector(31 downto 0)
           );
    end component;
    
    component MEM_WB_Register is
        port(
            clk:            in  std_logic;
            reset:          in  std_logic;
            rd_wr:          in  std_logic;
            mem_to_reg:     in  std_logic;
            wb_rd:          in  std_logic_vector(4 downto 0);
            alu_res:        in  std_logic_vector(31 downto 0);
            mem_data:       in  std_logic_vector(31 downto 0);
            rd_wr_out:      out std_logic;
            mem_to_reg_out: out std_logic;
            wb_rd_out:      out std_logic_vector(4 downto 0);
            alu_res_out:    out std_logic_vector(31 downto 0);
            mem_data_out:   out std_logic_vector(31 downto 0)
           );
    end component;

    --Write/Read Controller Wires
    signal wr_cont_addr_out:        std_logic_vector(1 downto 0)            := "00";
    signal rd_cont_to_memwb_reg:    std_logic_vector(31 downto 0)           := x"00000000";
    signal miss_or_wr:              std_logic                               := '0';
    signal wr_cont_enable:          std_logic                               := '0';
    
    --Cache Controller Wires
    signal miss_to_mux:             std_logic                               := '0';
    signal wr_cont_to_cash_we:      std_logic_vector(3 downto 0)            := "0000";
    signal cache_addr_out:          std_logic_vector(9 downto 0)            := "0000000000";
    signal cache_data_out:          std_logic_vector(31 downto 0)           := x"00000000";

begin

    memmain_to_if   <= miss_to_mux;
    memmain_we_out   <= wr_cont_to_cash_we;
    memmain_enable    <= miss_to_mux or wr_cont_enable;

    wr_cont: write_controller port map(
        mem_op              => exmem_mem_op_in,
        alu_res             => exmem_alu_res_in,
        rs2_data            => exmem_rs2_data_in,
        wr_enable           => wr_cont_enable,
        addr_out            => wr_cont_addr_out,
        we_out              => wr_cont_to_cash_we,
        mem_data_out        => memmain_data_out
      );

    rd_cont: read_controller port map(
        addr                => wr_cont_addr_out,
        mem_op              => exmem_mem_op_in,
        cache_data          => cache_data_out,
        mem_data_out        => rd_cont_to_memwb_reg
       );
       
    m_mux: miss_mux port map(
        miss_sel            => miss_to_mux,
        cache_addr          => cache_addr_out,
        alu_addr            => exmem_alu_res_in,
        addr_out            => memmain_mux_addr_out
       );   
    
    cash_cont: cache_controller port map(
        clk                 => clk,
        reset               => reset,
        wr_in               => wr_cont_enable,
        addr_in             => exmem_alu_res_in,
        mem_data_in         => memmain_data_in,
        mem_ena_out         => miss_to_mux,
        mem_addr_out        => cache_addr_out,
        cache_data_out      => cache_data_out
       );
    
    memwb_reg: MEM_WB_Register port map(
        clk                 => clk,
        reset               => reset,
        rd_wr               => exmem_rd_wr_in,
        mem_to_reg          => exmem_mem_to_reg_in,
        wb_rd               => exmem_rd_in,
        alu_res             => exmem_alu_res_in,
        mem_data            => rd_cont_to_memwb_reg,
        rd_wr_out           => memwb_rd_wr_out,
        mem_to_reg_out      => memwb_mem_to_reg_out,
        wb_rd_out           => memwb_rd_out,
        alu_res_out         => memwb_alu_res_out,
        mem_data_out        => memwb_mem_data_out
       );
       
end Behavioral;
