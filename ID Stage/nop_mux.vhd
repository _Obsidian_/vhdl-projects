library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity nop_mux is
    port(
        sel:            in  std_logic;
        rd_wr_in:       in  std_logic;
        branch_in:      in  std_logic;
        mem_to_reg_in:  in  std_logic;
        mem_op_in:      in  std_logic_vector(3 downto 0);
        alu_src_in:     in  std_logic_vector(3 downto 0);
        alu_op_in:      in  std_logic_vector(8 downto 0); --
        rd_wr_out:      out std_logic;
        branch_out:     out std_logic;
        mem_to_reg_out: out std_logic;
        mem_op_out:     out std_logic_vector(3 downto 0);
        alu_src_out:    out std_logic_vector(3 downto 0);
        alu_op_out:     out std_logic_vector(8 downto 0) --
       );    
end nop_mux;

architecture Behavioral of nop_mux is

begin

    branch_out      <= branch_in when sel = '0' else '0';
    mem_to_reg_out  <= mem_to_reg_in when sel = '0' else '0';
    rd_wr_out       <= rd_wr_in when sel = '0' else '0';
    mem_op_out      <= mem_op_in when sel = '0' else (others => '0');
    alu_op_out      <= alu_op_in when sel = '0' else (others => '0');
    alu_src_out     <= alu_src_in when sel = '0' else (others => '0'); 
   
end Behavioral;