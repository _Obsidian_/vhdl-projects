library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity hazard_detector is
    port(
        clk:            in      std_logic;
        reset:          in      std_logic;
        ex_mem_op:      in      std_logic_vector(3 downto 0);
        ex_rd:          in      std_logic_vector(4 downto 0);
        id_rs1:         in      std_logic_vector(4 downto 0);
        id_rs2:         in      std_logic_vector(4 downto 0);
        if_reg_wr:      out     std_logic;
        nop_control:    out     std_logic;
        pc_wr:          out     std_logic
       );       
end hazard_detector;

architecture Behavioral of hazard_detector is

    signal stall_sig:       std_logic := '0';

begin

    haz_det: process(id_rs1, id_rs2, clk)
    begin
         if (ex_mem_op(2) = '1' and stall_sig = '0') then
             if (id_rs1 = ex_rd) or (id_rs2 = ex_rd) then
                 stall_sig <= '1';
             end if;
         end if;
        
        if rising_edge(clk) and stall_sig = '1' then
                stall_sig <= '0';
        end if;
        
    end process;
    
    pc_wr       <= '0' when stall_sig = '1' else '1';
    nop_control <= '1' when stall_sig = '1' else '0';
    if_reg_wr   <= '0' when stall_sig = '1' else '1';

end Behavioral;
