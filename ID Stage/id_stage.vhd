library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity id_stage is
    port(
        clk:               in      std_logic;
        reset:             in      std_logic;
        wb_rd_wr:          in      std_logic;
        exid_mem_op:       in      std_logic_vector(3 downto 0);
        exid_mem_rd:       in      std_logic_vector(4 downto 0);
        wb_rd:             in      std_logic_vector(4 downto 0);
        ifid_ins_in:       in      std_logic_vector(31 downto 0);
        ifid_pc_in:        in      std_logic_vector(31 downto 0);
        wb_rd_data:        in      std_logic_vector(31 downto 0);
        id_if_reg_wr:      out     std_logic;
        id_pc_wr:          out     std_logic;
        id_branch_out:     out     std_logic;
        id_rd_wr_out:      out     std_logic;
        id_mem_to_reg_out: out     std_logic;
        id_hazard:         out     std_logic;
        id_alu_src_out:    out     std_logic_vector(3 downto 0);
        id_mem_op_out:     out     std_logic_vector(3 downto 0);
        id_alu_op_out:     out     std_logic_vector(8 downto 0);
        id_rd_addr_out:    out     std_logic_vector(4 downto 0);
        id_rs1_addr_out:   out     std_logic_vector(4 downto 0);
        id_rs2_addr_out:   out     std_logic_vector(4 downto 0);
        id_imm_value_out:  out     std_logic_vector(31 downto 0);
        id_rs1_data_out:   out     std_logic_vector(31 downto 0);
        id_rs2_data_out:   out     std_logic_vector(31 downto 0);
        id_ins_out:        out     std_logic_vector(31 downto 0);
        id_pc_out:         out     std_logic_vector(31 downto 0)
       );        
end id_stage;

architecture Behavioral of id_stage is

    component control_module is
        port(
            ins_in:         in  std_logic_vector(31 downto 0);
            branch:         out std_logic; 
            rd_wr:          out std_logic; 
            mem_to_reg:     out std_logic; 
            mem_op:         out std_logic_vector(3 downto 0);
            alu_src:        out std_logic_vector(3 downto 0);
            alu_op:         out std_logic_vector(8 downto 0)
           );
    end component;
    
    component hazard_detector is
        port(
            clk:            in      std_logic;
            reset:          in      std_logic;
            ex_mem_op:      in      std_logic_vector(3 downto 0); 
            ex_rd:          in      std_logic_vector(4 downto 0); 
            id_rs1:         in      std_logic_vector(4 downto 0); 
            id_rs2:         in      std_logic_vector(4 downto 0); 
            if_reg_wr:      out     std_logic;
            nop_control:    out     std_logic;
            pc_wr:          out     std_logic
           );
    end component;
    
    component nop_mux is
        port(
            sel:            in  std_logic;
            rd_wr_in:       in  std_logic;
            branch_in:      in  std_logic;
            mem_to_reg_in:  in  std_logic;
            mem_op_in:      in  std_logic_vector(3 downto 0);
            alu_src_in:     in  std_logic_vector(3 downto 0);
            alu_op_in:      in  std_logic_vector(8 downto 0);
            rd_wr_out:      out std_logic;
            branch_out:     out std_logic;
            mem_to_reg_out: out std_logic;
            mem_op_out:     out std_logic_vector(3 downto 0);
            alu_src_out:    out std_logic_vector(3 downto 0);
            alu_op_out:     out std_logic_vector(8 downto 0)
           );
     end component;
    
    component imm_value_generator is
        port(
            inst_in:        in  std_logic_vector(31 downto 0);
            imm_value_out:  out std_logic_vector(31 downto 0)
           );
    end component;
    
    component register_file is
        port(
            clk:        in      std_logic;
            reset:      in      std_logic;
            reg_wr:     in      std_logic;
            rs1_addr:   in      std_logic_vector(4 downto 0);
            rs2_addr:   in      std_logic_vector(4 downto 0);
            rd:         in      std_logic_vector(4 downto 0);
            rd_data:    in      std_logic_vector(31 downto 0);
            rs1_data:   out     std_logic_vector(31 downto 0);
            rs2_data:   out     std_logic_vector(31 downto 0)
           );
    end component;
    
    component ID_EX_Register is
        port(
            clk:            in      std_logic;
            reset:          in      std_logic;
            branch_in:      in      std_logic;
            rd_wr_in:       in      std_logic;
            mem_to_reg_in:  in      std_logic;
            alu_src_in:     in      std_logic_vector(3 downto 0);
            mem_op_in:      in      std_logic_vector(3 downto 0);
            alu_op_in:      in      std_logic_vector(8 downto 0);
            rd_addr_in:     in      std_logic_vector(4 downto 0);
            rs1_addr_in:    in      std_logic_vector(4 downto 0);
            rs2_addr_in:    in      std_logic_vector(4 downto 0);
            imm_value_in:   in      std_logic_vector(31 downto 0);
            rs1_data_in:    in      std_logic_vector(31 downto 0);
            rs2_data_in:    in      std_logic_vector(31 downto 0);
            ins_in:         in      std_logic_vector(31 downto 0);
            pc_in:          in      std_logic_vector(31 downto 0);      
            branch_out:     out     std_logic;
            rd_wr_out:      out     std_logic;
            mem_to_reg_out: out     std_logic;
            alu_src_out:    out     std_logic_vector(3 downto 0);
            mem_op_out:     out     std_logic_vector(3 downto 0);
            alu_op_out:     out     std_logic_vector(8 downto 0);
            rd_addr_out:    out     std_logic_vector(4 downto 0);
            rs1_addr_out:   out     std_logic_vector(4 downto 0);
            rs2_addr_out:   out     std_logic_vector(4 downto 0);
            imm_value_out:  out     std_logic_vector(31 downto 0);
            rs1_data_out:   out     std_logic_vector(31 downto 0);
            rs2_data_out:   out     std_logic_vector(31 downto 0);
            ins_out:        out     std_logic_vector(31 downto 0);
            pc_out:         out     std_logic_vector(31 downto 0)
          );
     end component;
     
     --Control Module Wires
     signal branch_ctrl_to_nop:         std_logic                           := '0';
     signal rd_wr_ctrl_to_nop:          std_logic                           := '0';
     signal mem_to_reg_ctrl_to_nop:     std_logic                           := '0';
     signal mem_op_ctrl_to_nop:         std_logic_vector(3 downto 0)        := "0000";
     signal alu_src_ctrl_to_nop:        std_logic_vector(3 downto 0)        := "0000";
     signal alu_op_ctrl_to_nop:         std_logic_vector(8 downto 0)        := "000000000";
     
     --NOP Mux Wires
     signal sel_haz_to_nop:             std_logic                           := '0';
     signal rd_wr_nop_to_idex:          std_logic                           := '0';
     signal branch_nop_to_idex:         std_logic                           := '0';
     signal mem_to_reg_nop_to_idex:     std_logic                           := '0';
     signal mem_op_nop_to_idex:         std_logic_vector(3 downto 0)        := "0000";
     signal alu_src_nop_to_idex:        std_logic_vector(3 downto 0)        := "0000";
     signal alu_op_nop_to_idex:         std_logic_vector(8 downto 0)        := "000000000";
     
     --Hazard Det Wires
     signal rs1_addr_data:              std_logic_vector(4 downto 0)        := "00000";
     signal rs2_addr_data:              std_logic_vector(4 downto 0)        := "00000";
     signal rd_addr_data:               std_logic_vector(4 downto 0)        := "00000";
     
     --Imm Val Gen Wires
     signal imm_val_gen_to_idex:        std_logic_vector(31 downto 0)       := x"00000000";
     
     --Reg File Wires
     signal rs1_data_regf_to_idex:      std_logic_vector(31 downto 0)       := x"00000000";
     signal rs2_data_regf_to_idex:      std_logic_vector(31 downto 0)       := x"00000000";
              
begin

    rs1_addr_data   <= ifid_ins_in(19 downto 15);
    rs2_addr_data   <= ifid_ins_in(24 downto 20);
    rd_addr_data    <= ifid_ins_in(11 downto 7);
    id_hazard       <= sel_haz_to_nop;
      
    cm: control_module port map(
       ins_in       => ifid_ins_in,
       branch       => branch_ctrl_to_nop,
       rd_wr        => rd_wr_ctrl_to_nop,
       mem_to_reg   => mem_to_reg_ctrl_to_nop,
       mem_op       => mem_op_ctrl_to_nop,
       alu_src      => alu_src_ctrl_to_nop,
       alu_op       => alu_op_ctrl_to_nop
      );
     
    nop: nop_mux port map(
       sel              => sel_haz_to_nop,
       rd_wr_in         => rd_wr_ctrl_to_nop,
       branch_in        => branch_ctrl_to_nop,
       mem_to_reg_in    => mem_to_reg_ctrl_to_nop,
       mem_op_in        => mem_op_ctrl_to_nop,
       alu_src_in       => alu_src_ctrl_to_nop,
       alu_op_in        => alu_op_ctrl_to_nop,
       rd_wr_out        => rd_wr_nop_to_idex,
       branch_out       => branch_nop_to_idex,
       mem_to_reg_out   => mem_to_reg_nop_to_idex,
       mem_op_out       => mem_op_nop_to_idex,
       alu_src_out      => alu_src_nop_to_idex,
       alu_op_out       => alu_op_nop_to_idex       
     );  
     
   haz_det: hazard_detector port map(
       clk              => clk,
       reset            => reset,
       ex_mem_op        => exid_mem_op,
       ex_rd            => exid_mem_rd,
       id_rs1           => rs1_addr_data,
       id_rs2           => rs2_addr_data,
       if_reg_wr        => id_if_reg_wr,
       nop_control      => sel_haz_to_nop,
       pc_wr            => id_pc_wr       
      );
     
    imm_val: imm_value_generator port map(
        inst_in         => ifid_ins_in,
        imm_value_out   => imm_val_gen_to_idex
       );
    
    reg_file: register_file port map(
        clk             => clk,
        reset           => reset,
        reg_wr          => wb_rd_wr,          
        rs1_addr        => rs1_addr_data,
        rs2_addr        => rs2_addr_data,
        rd              => wb_rd,
        rd_data         => wb_rd_data,
        rs1_data        => rs1_data_regf_to_idex,
        rs2_data        => rs2_data_regf_to_idex
       );
       
     id_ex: ID_EX_Register port map(
        clk             => clk,
        reset           => reset,
        branch_in       => branch_nop_to_idex,
        rd_wr_in        => rd_wr_nop_to_idex,
        mem_to_reg_in   => mem_to_reg_nop_to_idex,
        alu_src_in      => alu_src_nop_to_idex,
        mem_op_in       => mem_op_nop_to_idex,
        alu_op_in       => alu_op_nop_to_idex,
        rd_addr_in      => rd_addr_data,
        rs1_addr_in     => rs1_addr_data,
        rs2_addr_in     => rs2_addr_data,
        imm_value_in    => imm_val_gen_to_idex,
        rs1_data_in     => rs1_data_regf_to_idex,
        rs2_data_in     => rs2_data_regf_to_idex,
        ins_in          => ifid_ins_in,
        pc_in           => ifid_pc_in,
        branch_out      => id_branch_out,
        rd_wr_out       => id_rd_wr_out,
        mem_to_reg_out  => id_mem_to_reg_out,
        alu_src_out     => id_alu_src_out,
        mem_op_out      => id_mem_op_out,
        alu_op_out      => id_alu_op_out,
        rd_addr_out     => id_rd_addr_out,
        rs1_addr_out    => id_rs1_addr_out,
        rs2_addr_out    => id_rs2_addr_out,
        imm_value_out   => id_imm_value_out,
        rs1_data_out    => id_rs1_data_out,
        rs2_data_out    => id_rs2_data_out,
        ins_out         => id_ins_out,
        pc_out          => id_pc_out
       );

end Behavioral;