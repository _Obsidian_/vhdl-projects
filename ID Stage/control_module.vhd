library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity control_module is
    port(
        ins_in:                     in  std_logic_vector(31 downto 0);
        branch:                     out std_logic;
        rd_wr:                      out std_logic;
        mem_to_reg:                 out std_logic;
        mem_op:                     out std_logic_vector(3 downto 0);
        alu_src:                    out std_logic_vector(3 downto 0);
        alu_op:                     out std_logic_vector(8 downto 0) --
       );

end control_module;

architecture Behavioral of control_module is

    signal opcode:      std_logic_vector(6 downto 0)        := "0000000";
    signal funct3:      std_logic_vector(2 downto 0)        := "000";
    signal funct7:      std_logic                           := '0';

begin

    opcode <= ins_in(6 downto 0);
    funct3 <= ins_in(14 downto 12);
    funct7 <= ins_in(30);
    
    with (opcode) select
        branch <= '1' when "1100011" | "1101111" | "1100111",
                  '0' when others;
    
    with (opcode) select
        mem_to_reg <= '1' when "0000011" | "0110111",
                      '0' when others;
                      
    with (opcode) select
        rd_wr <= '0' when "1100011" | "0100011",
                 '1' when others;
    
    with (opcode & funct3) select
        mem_op <= "0100" when "0000011010",  -- lw
                  "0101" when "0000011001",  -- lh
                  "0110" when "0000011000",  -- lb
                  "1101" when "0000011101",  -- lhu
                  "1110" when "0000011100",  -- lbu
                  "1000" when "0100011010",  -- sw
                  "1001" when "0100011001",  -- sh
                  "1010" when "0100011000",  -- sb
                  "0000" when others; 
                  
    with (opcode) select
        alu_src <= "0000" when "0110011" | "1100011",               -- rs1 & rs2             --BEQ/NE/LT/GE/LTU/GEU, ADD, SUB, SLL, SLT/U, XOR, SRL, SRA, OR, AND 							
                   "0001" when "0010011" | "0000011",               -- rs1 & imm             --ADDI, SLTI/U, XORI, ORI, ANDI, SLLI, SRLI, SRAI, LB/H/W/BU/HU
				   "0010" when "0100011",                           -- rs2 & imm             --SB/H/W
                   "0011" when "0010111" | "1101111",               -- imm & pc              --JAL, AUIPC
                   "0100" when "0110111",                           -- imm 					 --LUI
                   "0101" when "1100111",		                    -- pc & 4                --JALR
                   "0110" when others;
      				  
	process(ins_in, opcode, funct3, funct7)
	   begin
	       case(opcode) is
	           when "1100011" =>
                    case(funct3) is
                        when "000" => alu_op <= "001000101";
                        when "001" => alu_op <= "100000010";
                        when "100" => alu_op <= "010000010";
                        when "101" => alu_op <= "011000010";
                        when "110" => alu_op <= "010100010";
                        when "111" => alu_op <= "011100010";
                        when others => alu_op <= "000000000";
                    end case;
               when "0000011" =>
                    case(funct3) is
                        when "000" | "001" | "010" => alu_op <= "000000001";
                        when "100" | "101" => alu_op <= "000100001";
                        when others => alu_op <= "000000000";
                    end case;
               when "0100011" =>
                    case(funct3) is
                        when "000" | "001" | "010" => alu_op <= "000000001";
                        when others => alu_op <= "000000000";
                    end case;
               when "0010011" =>
                    case(funct3) is
                        when "000" => alu_op <= "000000001";
                        when "010" => alu_op <= "010001000"; --
                        when "011" => alu_op <= "010101000"; --
                        when "100" => alu_op <= "000000101";
                        when "110" => alu_op <= "000000100";
                        when "111" => alu_op <= "000000011";
                        when "001" => alu_op <= "000000110";
                        when "101" =>
                            case(funct7) is
                                when '0' => alu_op <= "000000111";
                                when '1' => alu_op <= "000010111";
                                when others => alu_op <= "000000000";
                            end case;
                        when others => alu_op <= "000000000";
                   end case;
              when "0110011" =>
                case(funct3) is
                    when "000" =>
                        case(funct7) is
                            when '1' => alu_op <= "000000010";
                            when '0' => alu_op <= "000000001";
                            when others => alu_op <= "000000000";
                        end case;
                    when "001" => alu_op <= "000000110";
                    when "010" => alu_op <= "010001000"; --
                    when "011" => alu_op <= "010101000"; --
                    when "100" => alu_op <= "000000101";
                    when "101" =>
                        case(funct7) is
                            when '0' => alu_op <= "000000111";
                            when '1' => alu_op <= "000010111";
                            when others => alu_op <= "000000000";
                        end case;
                    when "110" => alu_op <= "000000100";
                    when "111" => alu_op <= "000000011";
                    when others => alu_op <= "000000000";
                end case;
             when "1100111" | "1101111" => alu_op <= "101000001";
             when "0010111" => alu_op <= "000000001";
             when "0110111" => alu_op <= "000000000";
             when others => alu_op <= "000000000";
        end case;
   end process;
   
end Behavioral;