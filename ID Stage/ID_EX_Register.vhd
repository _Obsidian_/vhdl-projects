library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ID_EX_Register is
    port(
        clk:            in      std_logic;
        reset:          in      std_logic;
        branch_in:      in      std_logic;
        rd_wr_in:       in      std_logic;
        mem_to_reg_in:  in      std_logic;
        alu_src_in:     in      std_logic_vector(3 downto 0);
        mem_op_in:      in      std_logic_vector(3 downto 0);
        alu_op_in:      in      std_logic_vector(8 downto 0); --
        rd_addr_in:     in      std_logic_vector(4 downto 0);
        rs1_addr_in:    in      std_logic_vector(4 downto 0);
        rs2_addr_in:    in      std_logic_vector(4 downto 0);
        imm_value_in:   in      std_logic_vector(31 downto 0);
        rs1_data_in:    in      std_logic_vector(31 downto 0);
        rs2_data_in:    in      std_logic_vector(31 downto 0);
        ins_in:         in      std_logic_vector(31 downto 0);
        pc_in:          in      std_logic_vector(31 downto 0);
        branch_out:     out     std_logic;
        rd_wr_out:      out     std_logic;
        mem_to_reg_out: out     std_logic;
        alu_src_out:    out     std_logic_vector(3 downto 0);
        mem_op_out:     out     std_logic_vector(3 downto 0);
        alu_op_out:     out     std_logic_vector(8 downto 0); --
        rd_addr_out:    out     std_logic_vector(4 downto 0);
        rs1_addr_out:   out     std_logic_vector(4 downto 0);
        rs2_addr_out:   out     std_logic_vector(4 downto 0);
        imm_value_out:  out     std_logic_vector(31 downto 0);
        rs1_data_out:   out     std_logic_vector(31 downto 0);
        rs2_data_out:   out     std_logic_vector(31 downto 0);
        ins_out:        out     std_logic_vector(31 downto 0);
        pc_out:         out     std_logic_vector(31 downto 0)    
       );
end ID_EX_Register;

architecture Behavioral of ID_EX_Register is

    signal pc_buff:              std_logic_vector (31 downto 0) := (others => '0');
    signal rs1_data_buff:        std_logic_vector (31 downto 0) := (others => '0');
    signal rs2_data_buff:        std_logic_vector (31 downto 0) := (others => '0');
    signal imm_value_buff:       std_logic_vector (31 downto 0) := (others => '0');
    signal ins_buff:             std_logic_vector (31 downto 0) := (others => '0');
    signal rd_wr_buff:           std_logic := '0';
    signal branch_buff:          std_logic := '0';
    signal mem_to_reg_buff:      std_logic := '0';
    signal mem_op_buff:          std_logic_vector (3 downto 0) := (others => '0');
    signal alu_op_buff:          std_logic_vector (8 downto 0) := (others => '0');
    signal alu_src_buff:         std_logic_vector (3 downto 0) := (others => '0');
    signal rs1_addr_buff:        std_logic_vector (4 downto 0) := (others => '0');
    signal rs2_addr_buff:        std_logic_vector (4 downto 0) := (others => '0');
    signal rd_addr_buff:         std_logic_vector (4 downto 0) := (others => '0');

begin

    process(clk, reset)
        begin
            if(rising_edge(clk)) then
                    branch_buff      <= branch_in;
                    rd_wr_buff       <= rd_wr_in;
                    mem_to_reg_buff  <= mem_to_reg_in;
                    alu_src_buff     <= alu_src_in;
                    mem_op_buff      <= mem_op_in;
                    alu_op_buff      <= alu_op_in;
                    rd_addr_buff     <= rd_addr_in;
                    rs1_addr_buff    <= rs1_addr_in;
                    rs2_addr_buff    <= rs2_addr_in;
                    imm_value_buff   <= imm_value_in;
                    rs1_data_buff    <= rs1_data_in;
                    rs2_data_buff    <= rs2_data_in;
                    ins_buff         <= ins_in;      
                    pc_buff          <= pc_in;
           end if;
      end process;
           
            pc_out          <= pc_buff when reset = '0' else (others => '0');
            rs1_data_out    <= rs1_data_buff when reset = '0' else (others => '0');
            rs2_data_out    <= rs2_data_buff when reset = '0' else (others => '0');
            imm_value_out   <= imm_value_buff when reset = '0' else (others => '0');
            rd_wr_out       <= rd_wr_buff when reset = '0' else  '0';
            branch_out      <= branch_buff when reset = '0' else '0';
            mem_to_reg_out  <= mem_to_reg_buff when reset = '0' else '0';
            mem_op_out      <= mem_op_buff when reset = '0' else (others => '0');
            alu_op_out      <= alu_op_buff when reset = '0' else (others => '0');
            alu_src_out     <= alu_src_buff when reset = '0' else (others => '0');
            rs1_addr_out    <= rs1_addr_buff when reset = '0' else (others => '0');
            rs2_addr_out    <= rs2_addr_buff when reset = '0' else (others => '0');
            rd_addr_out     <= rd_addr_buff when reset = '0' else (others => '0');
            rd_addr_out     <= rd_addr_buff when reset = '0' else (others => '0');
            ins_out         <= ins_buff when reset = '0' else (others => '0');
            
end Behavioral;