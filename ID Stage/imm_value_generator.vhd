library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity imm_value_generator is
    port(
        inst_in:        in  std_logic_vector(31 downto 0);
        imm_value_out:  out std_logic_vector(31 downto 0)
       );   
end imm_value_generator;

architecture Behavioral of imm_value_generator is

    signal funct7:      std_logic                               := '0';
    signal op_code:     std_logic_vector(6 downto 0)            := "0000000";
    signal slv_32:      std_logic_vector(31 downto 0)           := x"00000000";
    signal bit_S:       std_logic_vector(11 downto 0)           := "000000000000";
    signal bit_B:       std_logic_vector(11 downto 0)           := "000000000000";
    signal bit_J:       std_logic_vector(19 downto 0)           := "00000000000000000000";
    
begin

    funct7      <= inst_in(30);
    op_code     <= inst_in(6 downto 0);
    bit_S       <= inst_in(31 downto 25) & inst_in(11 downto 7);
    bit_B       <= std_logic_vector(shift_left(signed((inst_in(31) & inst_in(7) & inst_in(30 downto 25) & inst_in(11 downto 8))), 1));
    bit_J       <= std_logic_vector(shift_left(signed((inst_in(31) & inst_in(19 downto 12) & inst_in(20) & inst_in(30 downto 21))), 1));

    process(inst_in, funct7, op_code, bit_S, bit_B, bit_J)
        begin
            case (op_code) is
                when "0000011" | "1100111" =>
                    imm_value_out <= std_logic_vector(resize(signed(inst_in(31 downto 20)), slv_32'length));
                when "0010011" =>
                    if (funct7 = '0') then
                        imm_value_out <= std_logic_vector(resize(signed(inst_in(31 downto 20)), slv_32'length));
                    else
                        imm_value_out <= std_logic_vector(resize(signed(inst_in(29 downto 20)), slv_32'length));
                    end if;
                when "0010111" | "0110111" =>
                    imm_value_out <= std_logic_vector(shift_left(resize(signed(inst_in(31 downto 12)), slv_32'length), 12));
                when "0100011" =>
                    imm_value_out <= std_logic_vector(resize(signed(bit_S), slv_32'length));
                when "1100011" =>
                    imm_value_out <= std_logic_vector(resize(signed(bit_B), slv_32'length));
                when "1101111" =>
                    imm_value_out <= std_logic_vector(resize(signed(bit_J), slv_32'length));
                when others =>
                    imm_value_out <= x"00000000";
            end case;
    end process;    

end Behavioral;  