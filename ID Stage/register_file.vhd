library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity register_file is
    port(
        clk:        in      std_logic;
        reset:      in      std_logic;
        reg_wr:     in      std_logic;
        rs1_addr:   in      std_logic_vector(4 downto 0);
        rs2_addr:   in      std_logic_vector(4 downto 0);
        rd:         in      std_logic_vector(4 downto 0);
        rd_data:    in      std_logic_vector(31 downto 0);
        rs1_data:   out     std_logic_vector(31 downto 0);
        rs2_data:   out     std_logic_vector(31 downto 0)
       ); 

end register_file;

architecture Behavioral of register_file is

    type reg_type is array (0 to 31) of std_logic_vector (31 downto 0);
    
    signal reg_array:   reg_type  := (others => (others => '0'));
--        (x"00000000",     --X0
--         x"00000064",     --X1
--         x"000000C8",     --X2
--         x"FFFFFED4",     --X3
--         x"00000064",     --X4
--         x"000000C8",     --X5
--         x"FFFFFED4",     --X6
--         x"DEADBEEF",     --X7
--         x"CAFEDEAF",     --X8
--         x"DEAFFADE",     --X9
--         x"BADFACED",     --X10
--         x"FEDDECAF",     --X11
--         x"0ACECAFE",     --X12
--         x"ACEDFAD0",     --X13
--         x"1BEDFACE",     --X14
--         x"BEADFADE",     --X15
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF",
--         x"DEADBEEF");    

begin

    process(clk)
        begin
            if(rising_edge(clk)) then
                if(reg_wr = '1') then
                    reg_array(to_integer(unsigned(rd))) <= rd_data;
                end if;
            end if;
     end process;
     
     rs1_data <= reg_array(to_integer(unsigned(rs1_addr))) when reset = '0' else (others => '0');
     rs2_data <= reg_array(to_integer(unsigned(rs2_addr))) when reset = '0' else (others => '0');
