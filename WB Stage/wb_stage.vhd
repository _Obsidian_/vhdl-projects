library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity wb_stage is
    port(
        memwb_mem_to_reg:   in  std_logic;
        memwb_alu_res:      in  std_logic_vector(31 downto 0);
        memwb_mem_data:     in  std_logic_vector(31 downto 0);
        wb_data_out:        out std_logic_vector(31 downto 0)
    );
end wb_stage;

architecture Behavioral of wb_stage is

begin

    wb_data_out <= memwb_alu_res when (memwb_mem_to_reg = '0') else memwb_mem_data;

end Behavioral;
