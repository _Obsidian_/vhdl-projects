library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity EX_MEM_Register is
    port(
        clk:                    in  std_logic;
        reset:                  in  std_logic;
        rd_wr_in:               in  std_logic;
        andbranch_in:           in  std_logic;
        idex_mem_to_reg_in:     in  std_logic;
        mem_op_in:              in  std_logic_vector(3 downto 0);
        rd_in:                  in  std_logic_vector(4 downto 0);
        alu_res_in:             in  std_logic_vector(31 downto 0);
        branch_addr_in:         in  std_logic_vector(31 downto 0);
        rs2_data_in:            in  std_logic_vector(31 downto 0);
        rd_wr_out:              out std_logic;
        andbranch_out:          out std_logic;
        exmem_mem_to_reg_out:   out std_logic;
        mem_op_out:             out std_logic_vector(3 downto 0);
        rd_out:                 out std_logic_vector(4 downto 0);
        alu_res_out:            out std_logic_vector(31 downto 0);
        branch_addr_out:        out std_logic_vector(31 downto 0);
        rs2_data_out:           out std_logic_vector(31 downto 0)
    );
end EX_MEM_Register;

architecture Behavioral of EX_MEM_Register is

    signal rd_wr_buff:                  std_logic       := '0';
    signal exmem_mem_to_reg_buff:       std_logic       := '0';
    signal andbranch_buff:              std_logic       := '0';
    signal mem_op_buff:                 std_logic_vector(3 downto 0)        := (others => '0');
    signal rd_buff:                     std_logic_vector(4 downto 0)        := (others => '0');
    signal alu_res_buff:                std_logic_vector(31 downto 0)       := (others => '0');
    signal branch_addr_buff:            std_logic_vector(31 downto 0)       := (others => '0');
    signal rs2_data_buff:               std_logic_vector(31 downto 0)       := (others => '0');

begin

    process(clk, reset)
        begin
            if(rising_edge(clk)) then
                    rd_wr_buff               <= rd_wr_in;
                    andbranch_buff           <= andbranch_in;
                    exmem_mem_to_reg_buff    <= idex_mem_to_reg_in;
                    mem_op_buff              <= mem_op_in;
                    rd_buff                  <= rd_in;
                    alu_res_buff             <= alu_res_in;
                    branch_addr_buff         <= branch_addr_in;
                    rs2_data_buff            <= rs2_data_in;
           end if;
     end process;
     
    rd_wr_out            <= rd_wr_buff when reset = '0' else '0';
    andbranch_out        <= andbranch_buff when reset = '0' else '0';
    exmem_mem_to_reg_out <= exmem_mem_to_reg_buff when reset = '0' else '0';
    mem_op_out           <= mem_op_buff when reset = '0' else (others => '0');
    rd_out               <= rd_buff when reset = '0' else (others => '0');
    alu_res_out          <= alu_res_buff when reset = '0' else (others => '0');
    branch_addr_out      <= branch_addr_buff when reset = '0' else (others => '0');
    rs2_data_out         <= rs2_data_buff when reset = '0' else (others => '0');

end Behavioral;