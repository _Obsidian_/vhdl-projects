library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity address_adder is
    port(
        branch_in:          in  std_logic;
        id_rs1_in:          in  std_logic_vector(31 downto 0); --
        branch_addr_ex:     in  std_logic_vector(31 downto 0);
        imm_in:             in  std_logic_vector(31 downto 0);
        ins_in:             in  std_logic_vector(31 downto 0);
        pc_in:              in  std_logic_vector(31 downto 0);
        branch_out:         out std_logic;
        branch_addr:        out std_logic_vector(31 downto 0)
    );
end address_adder;

architecture Behavioral of address_adder is

    signal branch_address:      std_logic_vector(31 downto 0)   := x"00000000";
    signal branch_count:        unsigned(1 downto 0)            := "00";
    signal opcode:              std_logic_vector(6 downto 0)    := "0000000";

begin

        opcode <= ins_in(6 downto 0);
                        
        process(branch_in, branch_addr_ex, imm_in, pc_in, branch_address, opcode)
            begin 
                if(branch_in = '1') then
                    branch_count <= branch_count + 1;
                    if(branch_count = "01") then                                --Last instruction was a branch
                        if(opcode = "1101111") or (opcode = "1100111") then
                            branch_out <= '0';                            
                        end if;
                        branch_out <= '0';
                        branch_address <= branch_addr_ex;
                        branch_count <= "00";
                    end if;
                else
                    if(opcode = "1100111") then
                        branch_address <= imm_in + id_rs1_in;
                        branch_out <= '1';
                    else
                        branch_address <= imm_in + pc_in;
                        branch_out <= '1';
                    end if;
                end if;
                
                branch_addr <= branch_address;
                
        end process;

end Behavioral;
