library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity forwarding_unit is
    port(
        mem_wr:         in  std_logic;
        wb_wr:          in  std_logic;
        alu_src:        in  std_logic_vector(3 downto 0);
        mem_rd:         in  std_logic_vector(4 downto 0);
        rs1:            in  std_logic_vector(4 downto 0);
        rs2:            in  std_logic_vector(4 downto 0);
        wb_rd:          in  std_logic_vector(4 downto 0);
        alu_a_sel:      out std_logic_vector(2 downto 0);
        alu_b_sel:      out std_logic_vector(2 downto 0)
       );
end forwarding_unit;

architecture Behavioral of forwarding_unit is

begin
        
    process(mem_wr, wb_wr, alu_src, mem_rd, rs1, rs2, wb_rd)
        begin
            -- Ex/Mem Hazard
            if((mem_wr = '1') and (mem_rd /= "00000") and (mem_rd = rs1)) then
                 case (alu_src) is
                    when "0000" =>                                              -- branch or arithm (no imm)
                        alu_a_sel <= "001"; -- (mem_alu_res)
                        alu_b_sel <= "000"; -- (rs2)
                    when "0001" =>                                              --arithm (imm), load
                        alu_a_sel <= "001"; -- (mem_alu_res)
                        alu_b_sel <= "011"; -- (imm)
                    when "0101" =>
                        alu_a_sel <= "011";
                        alu_b_sel <= "100";
                    when others =>
                        alu_a_sel <= "000";
                        alu_b_sel <= "000"; 
                  end case;
             elsif((mem_wr = '1') and (mem_rd /= "00000") and (mem_rd = rs2)) then
                 case (alu_src) is
                    when "0000" =>                                              -- branch or arithm (no imm)
                        alu_a_sel <= "000"; -- (rs1)
                        alu_b_sel <= "001"; -- (mem_alu_res)
                    when "0010" =>                                              -- stores
                        alu_a_sel <= "100"; -- imm
                        alu_b_sel <= "001"; -- (mem_alu_res)
                    when "0101" =>
                        alu_a_sel <= "011";
                        alu_b_sel <= "100";
                    when others =>
                        alu_a_sel <= "000";
                        alu_b_sel <= "000"; 
                  end case;
             -- Mem/Wb Hazard
             elsif((wb_wr = '1') and (wb_rd /= "00000") and not ((mem_wr = '1') and (mem_rd /= "00000") and (mem_rd = rs1)) and (wb_rd = rs1)) then 
                case (alu_src) is
                    when "0000" =>                                              -- branch or arithm (no imm)
                        alu_a_sel <= "010"; -- (wb_data)
                        alu_b_sel <= "000"; -- (rs2)
                    when "0001" =>                                              --arithm (imm), load
                        alu_a_sel <= "010"; -- (wb_data)
                        alu_b_sel <= "011"; -- (imm)
                    when "0101" =>
                        alu_a_sel <= "011";
                        alu_b_sel <= "100";
                    when others =>
                        alu_a_sel <= "000";
                        alu_b_sel <= "000"; 
                  end case;
             elsif((wb_wr = '1') and (wb_rd /= "00000") and not ((mem_wr = '1') and (mem_rd /= "00000") and (mem_rd = rs2)) and (wb_rd = rs2)) then 
                case (alu_src) is
                    when "0000" =>                                              -- branch or arithm (no imm)
                        alu_a_sel <= "000"; -- (rs1)
                        alu_b_sel <= "010"; -- (wb_data)
                    when "0010" =>                                              -- stores
                        alu_a_sel <= "100"; -- (imm)
                        alu_b_sel <= "010"; -- (wb_data)
                    when "0101" =>
                        alu_a_sel <= "011";
                        alu_b_sel <= "100";
                    when others =>
                        alu_a_sel <= "000";
                        alu_b_sel <= "000"; 
                  end case;
             else                                                               --normal ops and other cases.
                case (alu_src) is
                    when "0001" =>
                        alu_a_sel <= "000"; -- (rs1)                            --arithm (imm), load
                        alu_b_sel <= "011"; -- (imm)
                    when "0010" =>
                        alu_a_sel <= "000"; --(rs1)                             --sw/h/b
                        alu_b_sel <= "011"; --(imm)
                    when "0011" =>                                              -- jal, auipc
                        alu_a_sel <= "011"; --(pc)
                        alu_b_sel <= "011"; --(imm)
                    when "0100" =>                                              -- lui
                        alu_a_sel <= "100"; --(imm to imm mux)
                        alu_b_sel <= "100"; --(imm)
                    when "0101" =>
                        alu_a_sel <= "011"; --(pc)                              -- jalr
                        alu_b_sel <= "100"; --4
                    when others =>
                        alu_a_sel <= "000";
                        alu_b_sel <= "000";
                end case;
             end if;
     end process;                        

end Behavioral;
