library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity alu_src_mux is
    port(
        sel:        in  std_logic_vector(2 downto 0); 
        port_0:     in  std_logic_vector(31 downto 0);
        port_1:     in  std_logic_vector(31 downto 0);
        port_2:     in  std_logic_vector(31 downto 0);
        port_3:     in  std_logic_vector(31 downto 0);
        port_4:     in  std_logic_vector(31 downto 0);
        mux_out:    out std_logic_vector(31 downto 0)
    );
end alu_src_mux;

architecture Behavioral of alu_src_mux is

begin

    process(sel, port_0, port_1, port_2, port_3)
        begin
            case (sel) is
                when "000" => mux_out <= port_0;
                when "001" => mux_out <= port_1;
                when "010" => mux_out <= port_2;
                when "011" => mux_out <= port_3;
                when "100" => mux_out <= port_4;
                when others => mux_out <= x"00000000";
            end case;
    end process;

end Behavioral;