library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity alu_32b is
    port(
        alu_ctrl:       in  std_logic_vector(8 downto 0); --
        input_a:        in  std_logic_vector(31 downto 0);
        input_b:        in  std_logic_vector(31 downto 0);
        alu_set:        out std_logic;
        alu_result:     out std_logic_vector(31 downto 0)
       );
end alu_32b;

architecture Behavioral of alu_32b is

    signal res:         std_logic_vector(32 downto 0)       := "000000000000000000000000000000000";   
    signal zero:        std_logic                           := '0';
    signal neg:         std_logic                           := '0';
    signal high:        std_logic                           := '1';


begin

    neg <= res(32);
    alu_result <= (res(31 downto 1) & '0') when alu_ctrl = "00001001" else res(31 downto 0);                    --If JALR set LSB of res to '0', else res(31 downto 0);
    zero <= '1' when res(31 downto 0) = x"00000000" else '0';
    
    with (alu_ctrl(8 downto 6)) select
        alu_set <= zero                     when "001",
                   neg                      when "010",
                   zero or not(zero or neg) when "011",
                   not zero                 when "100",
                   high                     when "101",
                   '0'                      when others;
                 
    process(alu_ctrl, input_a, input_b, res, zero, neg, high)
        begin       
            case (alu_ctrl(3 downto 0)) is
                when "0001" =>
                    if(alu_ctrl(5 downto 4) = "10") then
                        res <= std_logic_vector(unsigned('0' & input_a) + unsigned('0' & input_b));
                    else
                        res <= std_logic_vector(signed(input_a(31) & input_a) + signed(input_b(31) & input_b));
                    end if;
                when "0010" =>
                    if(alu_ctrl(5 downto 4) = "10") then
                        res <= std_logic_vector(unsigned('0' & input_a) - unsigned('0' & input_b));
                    else
                        res <= std_logic_vector(signed(input_a(31) & input_a) - signed(input_b(31) & input_b)); 
                    end if;
                when "0011" =>
                    res <= std_logic_vector(signed(input_a(31) & input_a) and signed(input_b(31) & input_b));
                when "0100" =>
                    res <= std_logic_vector(signed(input_a(31) & input_a) or signed(input_b(31) & input_b));
                when "0101" =>
                    res <= std_logic_vector(signed(input_a(31) & input_a) xor signed(input_b(31) & input_b));
                when "0110" =>
                      res <= std_logic_vector(shift_left(signed(input_a(31) & input_a), to_integer(unsigned(input_b(4 downto 0)))));
                when "0111" =>
                    if(alu_ctrl(5 downto 4) = "01") then
                        res <= std_logic_vector(shift_right(signed(input_a(31) & input_a), to_integer(unsigned(input_b(4 downto 0)))));
                    else
                        res <= std_logic_vector(shift_right(unsigned(input_a(31) & input_a), to_integer(unsigned(input_b(4 downto 0)))));
                    end if;
                when "1000" =>
                    if(alu_ctrl(5 downto 4) = "10") then
                        res <= std_logic_vector(unsigned('0' & input_a) - unsigned('0' & input_b));
                        if(res < ('0' & input_b)) then
                            res <= "000000000000000000000000000000001";
                        else
                            res <= "000000000000000000000000000000000";
                        end if;
                    else
                        res <= std_logic_vector(signed(input_a(31) & input_a) - signed(input_b(31) & input_b));
                        if(res < (input_b(31) & input_b)) then
                            res <= "000000000000000000000000000000001";
                        else
                            res <= "000000000000000000000000000000000";
                        end if;
                    end if;
                when "0000" =>
                    res <= std_logic_vector(signed(input_a(31) & input_a)); 
                when others =>
                    res <= (others => '0');
            end case;
     end process;
            
end Behavioral;