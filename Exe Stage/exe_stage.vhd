library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity exe_stage is
    port(
        clk:                    in  std_logic;
        reset:                  in  std_logic;
        idex_branch:            in  std_logic;
        idex_mem_to_reg:        in  std_logic;
        idex_rd_wr:             in  std_logic;
        wb_wr:                  in  std_logic;
        idex_alu_src:           in  std_logic_vector(3 downto 0);
        idex_mem_op:            in  std_logic_vector(3 downto 0);
        idex_rd:                in  std_logic_vector(4 downto 0);
        idex_rs1:               in  std_logic_vector(4 downto 0);
        idex_rs2:               in  std_logic_vector(4 downto 0);
        wb_rd:                  in  std_logic_vector(4 downto 0);
        idex_alu_op:            in  std_logic_vector(8 downto 0);
        idex_imm_val:           in  std_logic_vector(31 downto 0);
        idex_ins_in:            in  std_logic_vector(31 downto 0);
        idex_pc:                in  std_logic_vector(31 downto 0);
        idex_rs1_data:          in  std_logic_vector(31 downto 0);
        idex_rs2_data:          in  std_logic_vector(31 downto 0);
        mem_alu_result:         in  std_logic_vector(31 downto 0);
        wb_data:                in  std_logic_vector(31 downto 0);
        exmem_branch:           out std_logic;
        exmem_rd_wr:            out std_logic;
        exmem_mem_to_reg_out:   out std_logic;
        exmem_mem_op:           out std_logic_vector(3 downto 0);
        exmem_rd:               out std_logic_vector(4 downto 0);
        exmem_alu_result:       out std_logic_vector(31 downto 0);
        exmem_branch_addr:      out std_logic_vector(31 downto 0);
        exmem_rs2_data:         out std_logic_vector(31 downto 0)
       );
end exe_stage;

architecture Behavioral of exe_stage is

    component forwarding_unit is
        port(
            mem_wr:         in  std_logic;
            wb_wr:          in  std_logic;
            alu_src:        in  std_logic_vector(3 downto 0);
            mem_rd:         in  std_logic_vector(4 downto 0);
            rs1:            in  std_logic_vector(4 downto 0);
            rs2:            in  std_logic_vector(4 downto 0);
            wb_rd:          in  std_logic_vector(4 downto 0);
            alu_a_sel:      out std_logic_vector(2 downto 0);
            alu_b_sel:      out std_logic_vector(2 downto 0)
          );
    end component;
    
    component alu_src_mux is
        port(
            sel:        in  std_logic_vector(2 downto 0); 
            port_0:     in  std_logic_vector(31 downto 0);
            port_1:     in  std_logic_vector(31 downto 0);
            port_2:     in  std_logic_vector(31 downto 0);
            port_3:     in  std_logic_vector(31 downto 0);
            port_4:     in  std_logic_vector(31 downto 0);
            mux_out:    out std_logic_vector(31 downto 0)
           );
    end component;
        
    component alu_32b is
        port(
            alu_ctrl:       in  std_logic_vector(8 downto 0);
            input_a:        in  std_logic_vector(31 downto 0);
            input_b:        in  std_logic_vector(31 downto 0);
            alu_set:        out std_logic;
            alu_result:     out std_logic_vector(31 downto 0)
           );
    end component;
    
    component address_adder is
        port(
            branch_in:          in  std_logic;
            id_rs1_in:          in  std_logic_vector(31 downto 0);
            branch_addr_ex:     in  std_logic_vector(31 downto 0);
            imm_in:             in  std_logic_vector(31 downto 0);
            ins_in:             in  std_logic_vector(31 downto 0);
            pc_in:              in  std_logic_vector(31 downto 0);
            branch_out:         out std_logic;
            branch_addr:        out std_logic_vector(31 downto 0)
          );
    end component;
    
    component EX_MEM_Register is
        port(
            clk:                    in  std_logic;
            reset:                  in  std_logic;
            rd_wr_in:               in  std_logic;
            andbranch_in:           in  std_logic;
            idex_mem_to_reg_in:     in  std_logic;
            mem_op_in:              in  std_logic_vector(3 downto 0);
            rd_in:                  in  std_logic_vector(4 downto 0);
            alu_res_in:             in  std_logic_vector(31 downto 0);
            branch_addr_in:         in  std_logic_vector(31 downto 0);
            rs2_data_in:            in  std_logic_vector(31 downto 0);
            rd_wr_out:              out std_logic;
            andbranch_out:          out std_logic;
            exmem_mem_to_reg_out:   out std_logic;
            mem_op_out:             out std_logic_vector(3 downto 0);
            rd_out:                 out std_logic_vector(4 downto 0);
            alu_res_out:            out std_logic_vector(31 downto 0);
            branch_addr_out:        out std_logic_vector(31 downto 0);
            rs2_data_out:           out std_logic_vector(31 downto 0)
           );
    end component;
    
    --EX Register to FU wires
    signal exmem_rd_wr_to_fwd:      std_logic                           := '0';
    signal exmem_rd_to_fwd:         std_logic_vector(4 downto 0)        := "00000";
    
    --FU wires
    signal alu_a_sel_src_mux:       std_logic_vector(2 downto 0)        := "000";
    signal alu_b_sel_src_mux:       std_logic_vector(2 downto 0)        := "000";
    
    --IMM Mux wires
    signal muxa_to_alu:             std_logic_vector(31 downto 0)       := x"00000000";
    signal muxb_to_alu:             std_logic_vector(31 downto 0)       := x"00000000";
    
    --Branch Adder wires
    signal br_addr_to_ex_reg:       std_logic_vector(31 downto 0)       := x"00000000";
    signal rs1_data_wire:           std_logic_vector(31 downto 0)       := x"00000000";
    
    --ALU wires
    signal alu_res_to_ex_reg:       std_logic_vector(31 downto 0)       := x"00000000";   
    signal anded_branch:            std_logic                           := '0';
    signal exreg_to_addr_add:       std_logic                           := '0';
    signal branch_addr_to_and_br:   std_logic                           := '1';
    signal alu_set_to_and:          std_logic                           := '0';
    signal plus_4:                  std_logic_vector(31 downto 0)       := x"00000004";
    
    signal exreg_to_branch_add:     std_logic_vector(31 downto 0) := x"00000000";
    
begin

    rs1_data_wire <= idex_rs1_data;
    exmem_branch <= exreg_to_addr_add;
    exmem_branch_addr <= exreg_to_branch_add;
    anded_branch <= alu_set_to_and and idex_branch and branch_addr_to_and_br;
    exmem_rd     <= exmem_rd_to_fwd;
    exmem_rd_wr  <= exmem_rd_wr_to_fwd;

    for_unit: forwarding_unit port map(
        mem_wr          => exmem_rd_wr_to_fwd,
        wb_wr           => wb_wr,
        alu_src         => idex_alu_src,
        mem_rd          => exmem_rd_to_fwd,
        rs1             => idex_rs1,
        rs2             => idex_rs2,
        wb_rd           => wb_rd,
        alu_a_sel       => alu_a_sel_src_mux,
        alu_b_sel       => alu_b_sel_src_mux
       );
       
    alu_mux_a: alu_src_mux port map(
        sel             => alu_a_sel_src_mux,
        port_0          => rs1_data_wire,
        port_1          => mem_alu_result,
        port_2          => wb_data,
        port_3          => idex_pc,
        port_4          => idex_imm_val,
        mux_out         => muxa_to_alu
       );
       
    alu_mux_b: alu_src_mux port map(
        sel             => alu_b_sel_src_mux,
        port_0          => idex_rs2_data,
        port_1          => mem_alu_result,
        port_2          => wb_data,
        port_3          => idex_imm_val,
        port_4          => plus_4,
        mux_out         => muxb_to_alu
       );

    branch_adder: address_adder port map(
        branch_in        => exreg_to_addr_add,
        id_rs1_in        => rs1_data_wire,
        branch_addr_ex   => exreg_to_branch_add,
        imm_in           => idex_imm_val,
        ins_in           => idex_ins_in,
        pc_in            => idex_pc,
        branch_out       => branch_addr_to_and_br,
        branch_addr      => br_addr_to_ex_reg
       );
    
    alu: alu_32b port map(
        alu_ctrl        => idex_alu_op,
        input_a         => muxa_to_alu,
        input_b         => muxb_to_alu,
        alu_set         => alu_set_to_and,
        alu_result      => alu_res_to_ex_reg
       );

    ex_mem_reg: EX_MEM_Register port map(
        clk                     => clk,
        reset                   => reset,
        rd_wr_in                => idex_rd_wr,
        andbranch_in            => anded_branch,
        idex_mem_to_reg_in      => idex_mem_to_reg,
        mem_op_in               => idex_mem_op,
        rd_in                   => idex_rd,
        alu_res_in              => alu_res_to_ex_reg,
        branch_addr_in          => br_addr_to_ex_reg,
        rs2_data_in             => idex_rs2_data,
        rd_wr_out               => exmem_rd_wr_to_fwd,
        andbranch_out           => exreg_to_addr_add,
        exmem_mem_to_reg_out    => exmem_mem_to_reg_out,
        mem_op_out              => exmem_mem_op,
        rd_out                  => exmem_rd_to_fwd,
        alu_res_out             => exmem_alu_result,
        branch_addr_out         => exreg_to_branch_add,
        rs2_data_out            => exmem_rs2_data
    );

end Behavioral;