library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Program_Counter is
    port (
        clk:            in  std_logic;
        reset:          in  std_logic;
        br_addr:        in  std_logic_vector(31 downto 0);
        pc_src:         in  std_logic;
        pc_wr:          in  std_logic;
        pc_reg:         out std_logic_vector(31 downto 0)
       );
end Program_Counter;

architecture Behavioral of Program_Counter is

	signal pc_counter	:	unsigned(31 downto 0)			:= x"00000000";

begin

    cnt_proc: process(clk, reset) is
    begin
        if rising_edge(clk) then
            if (reset = '1') then 
                pc_counter <= (others => '0');
            elsif (pc_wr = '1') then
                if (pc_src = '0') then
                    pc_counter <= pc_counter + 4;
                else
                    pc_counter <= unsigned(br_addr);
                end if;
            end if;
        end if;        
    end process;

     pc_reg <= std_logic_vector(pc_counter);    
    
end Behavioral;