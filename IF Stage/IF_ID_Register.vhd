library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity IF_ID_Register is
    port(
        clk:            in  std_logic;
        reset:          in  std_logic;
        pc_in:          in  std_logic_vector(31 downto 0);
        ins_in:         in  std_logic_vector(31 downto 0);
        if_id_wr:       in  std_logic;
        pc_out:         out std_logic_vector(31 downto 0);
        ins_out:        out std_logic_vector(31 downto 0)
       );

end IF_ID_Register;

architecture Behavioral of IF_ID_Register is

    signal pc_buff:     std_logic_vector(31 downto 0)       := x"00000000";
    signal ins_buff:    std_logic_vector(31 downto 0)       := x"00000000";

begin

    process(clk, reset)
    begin
        if(reset = '1') then
            pc_buff     <= x"00000000";
            ins_buff    <= x"00000000";
        elsif(rising_edge(clk)) then
            if(if_id_wr = '1') then
                pc_buff     <= pc_in;
                ins_buff    <= ins_in;
            end if;
        end if;
     end process;

pc_out  <= pc_buff when reset = '0' else (others => '0');
ins_out <= ins_buff when reset = '0' else (others => '0');

end Behavioral;
