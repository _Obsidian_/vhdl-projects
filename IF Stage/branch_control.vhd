library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity branch_control is
    port(
        clk:            in  std_logic;
        branch_in:      in  std_logic;
        flush_out:      out std_logic
       );
end branch_control;

architecture Behavioral of branch_control is

    signal branch_count:    unsigned(1 downto 0)   := "00";
    signal flushing:        std_logic              := '0';

begin

    process(clk)
        begin
            if ((branch_in = '1') and (flushing = '0')) then  --has to stay to flush register
                flushing <= '1';
            end if;
            if((rising_edge(clk)) and (flushing = '1')) then  
                if(branch_count = "01") then
                    flushing     <= '0';
                    branch_count <= "00";
                else
                    branch_count <= branch_count + 1;
                end if;
            end if;
    end process;

    flush_out <= flushing;

end Behavioral;
