library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity d_ff is
    port( 
        clk:  in std_logic;
        ena:  in std_logic;
        d:    in std_logic_vector(31 downto 0);
        q:    out std_logic_vector(31 downto 0)
       );
end d_ff;

architecture Behavioral of d_ff is

begin
    dff: process(clk)
        begin
            if rising_edge(clk) and ena = '1' then
                q <= d;
            end if;
    end process;
    
end Behavioral;
