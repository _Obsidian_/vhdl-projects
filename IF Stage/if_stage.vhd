library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity if_stage is
    port(
        clk:                in  std_logic;
        reset:              in  std_logic;
        ex_pc_src:          in  std_logic;
        id_pc_wr:           in  std_logic;
        id_if_id_wr:        in  std_logic;
        id_hazard:          in  std_logic;
        ins_in:             in  std_logic_vector(31 downto 0);
        ex_br_addr:         in  std_logic_vector(31 downto 0);
        if_ins_addr:        out std_logic_vector(31 downto 0);
        if_pc_out:          out std_logic_vector(31 downto 0);
        if_ins_out:         out std_logic_vector(31 downto 0)
       );
end if_stage;

architecture Behavioral of if_stage is

    component branch_control is
        port(
            clk:            in  std_logic;
            branch_in:      in  std_logic;
            flush_out:      out std_logic
           );
    end component;

    component Program_Counter is
        port(
            clk:            in  std_logic;
            reset:          in  std_logic;
            br_addr:        in  std_logic_vector(31 downto 0);
            pc_src:         in  std_logic;
            pc_wr:          in  std_logic;
            pc_reg:         out std_logic_vector(31 downto 0)
           );
    end component;
    
    component d_ff is
        port(
            clk:  in    std_logic;
            ena:  in    std_logic;
            d:    in    std_logic_vector(31 downto 0);
            q:    out   std_logic_vector(31 downto 0)
        );
    end component;
    
    component IF_ID_Register
        port(
            clk:            in  std_logic;
            reset:          in  std_logic;
            pc_in:          in  std_logic_vector(31 downto 0);
            ins_in:         in  std_logic_vector(31 downto 0);
            if_id_wr:       in  std_logic;
            pc_out:         out std_logic_vector(31 downto 0);
            ins_out:        out std_logic_vector(31 downto 0)
           );
     end component;
    
    -- Instruction Fetch Wires 
    signal branch_or:           std_logic                           := '0';
    signal branch_and:          std_logic                           := '0';   
    signal if_id_or:            std_logic                           := '0';
    signal reset_or:            std_logic                           := '0';  
    signal haz_and:             std_logic                           := '0';
    signal flush:               std_logic                           := '0';
    signal if_dff_ena:          std_logic                           := '0';
    signal pc_out_pc:           std_logic_vector(31 downto 0)       := x"00000000";
    signal doutb:               std_logic_vector(31 downto 0)       := x"00000000";
    signal dff1_to_ifid:        std_logic_vector(31 downto 0)       := x"00000000";
    signal douta:               std_logic_vector(31 downto 0)       := x"00000000";
           
begin
    
    branch_or   <= flush or reset;
    branch_and  <= not flush and if_dff_ena;
    if_ins_addr <= pc_out_pc;
    if_dff_ena  <= not id_hazard;
    
    bc: branch_control port map(
        clk          => clk,
        branch_in    => ex_pc_src,
        flush_out    => flush
    );
    
    d_ff1: d_ff port map(
        clk         => clk,
        ena         => if_dff_ena,
        d           => pc_out_pc,
        q           => dff1_to_ifid
       );

    pc: Program_Counter port map(
        clk         => clk,
        reset       => reset,
        br_addr     => ex_br_addr,
        pc_src      => ex_pc_src,
        pc_wr       => if_dff_ena,
        pc_reg      => pc_out_pc
       );
       
     IFID: IF_ID_Register port map(
        clk         => clk,
        reset       => branch_or,
        pc_in       => dff1_to_ifid,
        ins_in      => ins_in,
        if_id_wr    => branch_and,
        pc_out      => if_pc_out,
        ins_out     => if_ins_out
       );

end Behavioral;